async function loadAllMovie()
{
    var response =  await fetch('https://dv-excercise-backend.appspot.com/movies')
    let data = await response.json()
    return data
}
function createTableMovie(data)
{
    let resultElenment = document.getElementById('tablemovie')

    let tableNode = document.createElement('table')
    tableNode.setAttribute('class','table')
    resultElenment.appendChild(tableNode)

    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)

    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerHTML = 'Movie Name'
    tableHeadNode.appendChild(tableHeaderNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerHTML = 'Synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerHTML = 'Image'
    tableHeadNode.appendChild(tableHeaderNode)

    data.then((json) =>{
        for (let i=0;i < json.length; i++)
        {
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)

            columnNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src',currentData['imageUrl'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)

            var dataFirstColumnNode = document.createElement('th')
            dataFirstColumnNode.setAttribute('scope','row')
            dataFirstColumnNode.innerHTML = currentData['name']
            dataRow.appendChild(dataFirstColumnNode)

            var columnNode = null;
            columnNode = document.createElement('td')
            columnNode.innerHTML = currentData['synopsis']
            dataRow.appendChild(columnNode)
        }

    })
}
async function loadOneMovie()
{
    let searchmovie = document.getElementById('searchmv').value
    
    if(searchmovie != ' ' && searchmovie !=null)
    {
        let response = await fetch('https://dv-excercise-backend.appspot.com/movies/' +searchmovie)
        let data = await response.json()
        return data
    }
}
function createResultTableMV(data)
{
    let resultElenment = document.getElementById('result')
    
    data.then((json) =>{

            console.log(json)
            
            for(var i=0;i<json.length;i++)
            {
                var moviesList = json[i]
                noMv = i+1;
                var ProfileText = document.createElement('h1')
                ProfileText.innerHTML = 'MOVIE : '+noMv
                resultElenment.appendChild(ProfileText)
            
                var mvImage = document.createElement('img')
                mvImage.setAttribute('src',moviesList['imageUrl'])
                mvImage.style.width = '200px'
                mvImage.style.height = '200px'
                
                var name = document.createElement('h3')
                name.innerHTML = 'Moviename :'+moviesList['name']
                
                var synopsis = document.createElement('p')
                synopsis.innerHTML = moviesList['synopsis']

                resultElenment.appendChild(mvImage)
                resultElenment.appendChild(name)
                resultElenment.appendChild(synopsis)
            }
    })
}