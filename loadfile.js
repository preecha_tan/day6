"use strict"

async function loadAllStudents()
{
    let responseData = null
    var data = fetch('https://dv-student-backend-2019.appspot.com/students')
    .then((response) =>{
        console.log(response)
        return response.json()
    }).then((json)=> {
        responseData = json
        var resultElenment = document.getElementById('result')
        resultElenment.innerHTML = JSON.stringify(json,null,2)
    })  
}
async function loadAllStudentsAsync()
{
    let response = await fetch('https://dv-student-backend-2019.appspot.com/students')
    let data = await response.json()
        var resultElenment = document.getElementById('result')
        resultElenment.innerHTML = JSON.stringify(data,null,2)
        return data
}
function createResultTable(data)
{
    let resultElenment = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    tableNode.setAttribute('class','table')
    resultElenment.appendChild(tableNode)

    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)

    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerHTML = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerHTML = 'studentID'
    tableHeadNode.appendChild(tableHeaderNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerHTML = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerHTML = 'surname'
    tableHeadNode.appendChild(tableHeaderNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerHTML = 'gpa'
    tableHeadNode.appendChild(tableHeaderNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope','col')
    tableHeaderNode.innerHTML = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((json) =>{
        for (let i=0;i < json.length; i++)
        {
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFirstColumnNode = document.createElement('th')
            dataFirstColumnNode.setAttribute('scope','row')
            dataFirstColumnNode.innerHTML = currentData['id']
            dataRow.appendChild(dataFirstColumnNode)

            var columnNode = null;
            columnNode = document.createElement('td')
            columnNode.innerHTML = currentData['studentId']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerHTML = currentData['name']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerHTML = currentData['surname']
            dataRow.appendChild(columnNode)
            columnNode = document.createElement('td')
            columnNode.innerHTML = currentData['gpa']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src',currentData['image'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)

            
        }

    })
}
async function loadOneStudents()
{
    let studentId = document.getElementById('queryId').value
    
    if(studentId != ' ' && studentId !=null)
    {
        let response = await fetch('https://dv-student-backend-2019.appspot.com/students/' + studentId)
        let data = await response.json()
        // var resultElenment = document.getElementById('resultTable2')
        // resultElenment.innerHTML = JSON.stringify(data,null,2)
        return data
    }
}
function createResultTableOne(data)
{
    // let resultElenment = document.getElementById('resultTable2')
    // let tableNode = document.createElement('table')
    // tableNode.setAttribute('class','table')
    // resultElenment.appendChild(tableNode)

    // let tableHeadNode = document.createElement('thead')
    // tableNode.appendChild(tableHeadNode)

    // var tableRowNode = document.createElement('tr')
    // tableHeadNode.appendChild(tableRowNode)

    // var tableHeaderNode = document.createElement('th')
    // tableHeaderNode.setAttribute('scope','col')
    // tableHeaderNode.innerHTML = '#'
    // tableHeadNode.appendChild(tableHeaderNode)

    // var tableHeaderNode = document.createElement('th')
    // tableHeaderNode.setAttribute('scope','col')
    // tableHeaderNode.innerHTML = 'studentID'
    // tableHeadNode.appendChild(tableHeaderNode)

    // var tableHeaderNode = document.createElement('th')
    // tableHeaderNode.setAttribute('scope','col')
    // tableHeaderNode.innerHTML = 'name'
    // tableHeadNode.appendChild(tableHeaderNode)

    // var tableHeaderNode = document.createElement('th')
    // tableHeaderNode.setAttribute('scope','col')
    // tableHeaderNode.innerHTML = 'surname'
    // tableHeadNode.appendChild(tableHeaderNode)

    // var tableHeaderNode = document.createElement('th')
    // tableHeaderNode.setAttribute('scope','col')
    // tableHeaderNode.innerHTML = 'gpa'
    // tableHeadNode.appendChild(tableHeaderNode)

    // var tableHeaderNode = document.createElement('th')
    // tableHeaderNode.setAttribute('scope','col')
    // tableHeaderNode.innerHTML = 'image'
    // tableHeadNode.appendChild(tableHeaderNode)


    data.then((json) =>{
        // for (let i=0;i < json.length; i++)
        // {

            var Profile = document.getElementById('resultTable2')
            
            var wrapper = document.createElement('div')
            wrapper.setAttribute('id','wrapper')
            var oldwrapper = document.getElementById('wrapper')

            Profile.appendChild(wrapper)
            if(oldwrapper!=null)
            {
                Profile.removeChild(oldwrapper)
            }
            
            // var currentData = json

            var ProfileText = document.createElement('h1')
            ProfileText.innerHTML = 'My Profile'
            wrapper.appendChild(ProfileText)
            
            var Image = document.createElement('img')
            Image.setAttribute('src',json['image'])
            Image.style.width = '200px'
            Image.style.height = '200px'
            

            var stdID = document.createElement('p')
            stdID.innerHTML = 'studentId :'+json['studentId']

            var name = document.createElement('p')
            name.innerHTML = 'Name : '+json['name']+" "+json['surname']
            
            var gpa = document.createElement('p')
            gpa.innerHTML = 'GPA :'+json['gpa']

            var penAmount = document.createElement('p')
            penAmount.innerHTML = 'Pen mount : '+json['penAmount']
            var description = document.createElement('p')
            description.innerHTML = 'Description : '+json['description']


            wrapper.appendChild(Image)
            wrapper.appendChild(stdID)
            wrapper.appendChild(name)
            wrapper.appendChild(gpa)
            wrapper.appendChild(penAmount)
            wrapper.appendChild(description)

        



            
            // var dataRow = document.createElement('tr')
            // tableNode.appendChild(dataRow)
            // var dataFirstColumnNode = document.createElement('th')
            // dataFirstColumnNode.setAttribute('scope','row')
            // dataFirstColumnNode.innerHTML = currentData['id']
            // dataRow.appendChild(dataFirstColumnNode)

            // var columnNode = null;
            // columnNode = document.createElement('td')
            // columnNode.innerHTML = currentData['studentId']
            // dataRow.appendChild(columnNode)

            // columnNode = document.createElement('td')
            // columnNode.innerHTML = currentData['name']
            // dataRow.appendChild(columnNode)

            // columnNode = document.createElement('td')
            // columnNode.innerHTML = currentData['surname']
            // dataRow.appendChild(columnNode)
            // columnNode = document.createElement('td')
            // columnNode.innerHTML = currentData['gpa']
            // dataRow.appendChild(columnNode)

            // columnNode = document.createElement('td')
            // var imageNode = document.createElement('img')
            // imageNode.setAttribute('src',currentData['image'])
            // imageNode.style.width = '200px'
            // imageNode.style.height = '200px'
            // dataRow.appendChild(imageNode)
        // }

    })
}